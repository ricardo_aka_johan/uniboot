# Uniboot

Uniboot est un caractère typographique dessiné par [Ricardo aka Johan](http://ricardoakajohan.be/).

Tout commence par la découverte de [Prototypo](https://www.prototypo.io/)(1) qui permet d’après un squelette déjà existant de manipuler les variations d’un caractère. Poussant ces variations à leur maximum, le résultat donne une typographie accidentée contenant mille et un défauts. Mais réadaptant ses défauts, cela devient sa singularité. Tout ceci peaufiné & élaboré en s’inspirant de l’émoji « Chaussure Femme » — U+1F462 —.

(1)Outil professionnel permettant de créer des polices personnalisées.


## Specimen

![specimen1](Specimen1.jpg)
![specimen2](Specimen2.jpg)
![specimen3](Specimen3.jpg)
![specimen4](Specimen4.jpg)
![specimen5](Specimen5.jpg)
![specimen6](Specimen6.jpg)
![specimen7](Specimen7.jpg)

## License

Uniboot is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at
http://scripts.sil.org/OFL
